import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // declare some models for our data
  title = 'time for coffee';
  today = new Date()
  products = [{desc:'Pots', price:12.99},
              {desc:'Dots', price:3.99},
              {desc:'Spots', price:24.99}]
  flag = true
  showFlag = false
  myURL = 'https://via.placeholder.com/32'
  otherId = 'billgates'

  // declare listener functions
  handleClick(){
    console.log(event.target)
    this.flag = false
    this.showFlag = true
  }
  handleCustomEvent(event){
    console.log(event)
  }

}

