import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { runInThisContext } from 'vm';

@Component({
  selector: 'app-fb',
  templateUrl: './fb.component.html',
  styleUrls: ['./fb.component.css']
})
export class FbComponent implements OnInit {

  // declare models for this component
  @Input() id='mickeymouse'
  fbidUrl = 'https://graph.facebook.com/mickeymouse/picture?type=normal'

  @Output() myEvent = new EventEmitter()

  constructor() { }

  ngOnInit() {
  }

  resolveImageURL(event){
    // construct a new URL
    this.fbidUrl = `https://graph.facebook.com/${this.id}/picture?type=normal`
    // here we can choose to emit a custom event
    this.myEvent.emit( {value:this.fbidUrl}  )
  }


}
